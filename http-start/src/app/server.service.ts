import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ServerService {

  constructor(private http: Http) { }

  storeServers(servers: any []) {
    const headers = new Headers({'Content-Type':'application/json'});
    //return observables
    return this.http.put(
      'https://udemy-ng-http-9c34d.firebaseio.com/data.json', 
      servers, 
      { headers:headers }
    );
  }

  getServers() {
    return this.http.get('https://udemy-ng-http-9c34d.firebaseio.com/data')
    .map(
      (response : Response) => {
        const servers = response.json();
        for (const server of servers) {
            server.name = 'FETCHED_' + server.name;
        }
        return servers;
      }
    ).catch((error: Response) => {
      console.log(error);
      return Observable.throw(error);
    });
  }

  getAppName() {
    return this.http.get('https://udemy-ng-http-9c34d.firebaseio.com/appName.json').map(
      (response: Response) => {
        return response.json();
      }
    )
  }

}
