import { Component, Input, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css'],
  providers:[CounterService]
})
export class InactiveUsersComponent implements OnInit {

  users: string [];
  counter: number = 0;

  constructor(private usersService:UsersService, private counterService: CounterService) {
    this.counterService.emitter.subscribe(counter => this.counter = counter);
  }

  ngOnInit() {
    this.users = this.usersService.inactiveUsers;
  }

  onSetToActive(id: number) {
    this.usersService.setToActive(id);
    this.counterService.hitCounter();
  }

}
