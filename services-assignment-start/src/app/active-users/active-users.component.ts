import { Component, Input, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css'],
  providers: [CounterService]
})
export class ActiveUsersComponent implements OnInit {
  
  users: string [];
  counter: number = 0;

  constructor(private usersService: UsersService, private counterService: CounterService) { 
    this.counterService.emitter.subscribe(counter => this.counter = counter);
  }

  ngOnInit() {
    this.users = this.usersService.activeUsers;
  }
  
  onSetToInactive(id: number) {
    this.usersService.setToInactive(id);
    this.counterService.hitCounter();
  }

}
