import { EventEmitter } from "@angular/core";

export class CounterService {

    counter: number = 0;
    emitter = new EventEmitter<number>();

    hitCounter() {
        this.counter++;
        this.emitter.emit(this.counter);
    }

}