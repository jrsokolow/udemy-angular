import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  subscriptions: {key:string, value:string} [] = [
    {
      key: 'basic',
      value: 'Basic'
    },
    {
      key: 'advanced',
      value: 'Advanced'
    },
    {
      key: 'pro',
      value: 'Pro'
    }];
  data = {
    email:'',
    subscription:'',
    password:''
  };
  submitted: boolean = false;
  @ViewChild('form') form: NgForm;
  defaultSubscription: string = this.subscriptions[0].key;


  onSubmit() {
    this.data.email = this.form.value.email;
    this.data.subscription = this.form.value.subscription;
    this.data.password = this.form.value.password;
    this.submitted = true;
    this.form.reset();
  }

  // local referenced passed as argument
  // onSubmit(formData:NgForm) {
  //   console.log(formData.value);
  //   this.data.email = formData.value.email;
  //   this.data.subscription = formData.value.subscription;
  //   this.data.password = formData.value.password;
  //   this.submitted = true;
  //   formData.reset();
  // }
}
