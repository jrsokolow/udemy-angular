import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { WarningAlert } from './components/warning-alert/warning-alert.component';
import { SuccessAlert } from './components/success-alert/success-alert.component';
import { ServerComponent } from './components/server/server.component';
import { ServersComponent } from './components/servers/servers.component';
import { UserNameComponent } from './components/user-name/user-name.component';
import { AngularDirectiveComponent } from './components/angular-directive/angular-directive.component';
import { GameControlComponent } from './components/game-control/game-control.component';
import { OddComponent } from './components/odd/odd.component';
import { EvenComponent } from './components/even/even.component';
import { ClassTogglerDirective } from './directives/class-toggler.directive';
import { ClassToggler2Directive } from './directives/class-toggler2.directive';

@NgModule({
  declarations: [
    AppComponent,
    WarningAlert,
    SuccessAlert,
    ServerComponent,
    ServersComponent,
    UserNameComponent,
    AngularDirectiveComponent,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    ClassTogglerDirective,
    ClassToggler2Directive
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
