import { Directive, Input, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appClassToggler2]'
})
export class ClassToggler2Directive {

  @Input() className: string;
  @HostBinding('class') class: string = '';

  constructor(private elementRef: ElementRef) { }

  @HostListener('click') click(eventData: Event) {
    if(this.elementRef.nativeElement.classList.contains(this.className)) {
      this.class = '';
    } else {
      this.class = this.className;
    }
  }

}
