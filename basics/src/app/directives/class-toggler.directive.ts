import { Directive, Input, HostListener, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appClassToggler]'
})
export class ClassTogglerDirective {

  @Input() className: string

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  @HostListener('click') click(eventData: Event) {
    if(this.elementRef.nativeElement.classList.contains(this.className)) {
      this.renderer.removeClass(this.elementRef.nativeElement, this.className);
    } else {
      this.renderer.addClass(this.elementRef.nativeElement, this.className);
    }
  }

}
