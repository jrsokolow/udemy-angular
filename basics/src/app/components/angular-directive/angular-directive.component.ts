import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-angular-directive',
  templateUrl: './angular-directive.component.html',
  styleUrls: ['./angular-directive.component.css']
})
export class AngularDirectiveComponent implements OnInit {

  pDisplay: boolean = false
  clickRegister: Array<Boolean> = []

  constructor() { }

  ngOnInit() {
  }

  togglePdisplay() {
    this.clickRegister.push(this.pDisplay);
    this.pDisplay = !this.pDisplay
  }

}
