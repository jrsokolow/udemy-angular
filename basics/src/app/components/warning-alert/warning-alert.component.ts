import { Component } from "@angular/core";

@Component({
    selector:'app-warning',
    template:`<p>warning</p>`,
    styles: [ 'p { color: red; }']
})
export class WarningAlert {

}