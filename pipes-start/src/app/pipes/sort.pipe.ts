import { PipeTransform, Pipe } from "@angular/core";

@Pipe({
    name:'sort',
    pure: false
})
export class SortPipe implements PipeTransform {

    transform(value:[{instanceType: string, name: string, status: string, started: Date}]) {
        return value.sort((a, b) => {
            if(a.name < b.name) {
                return -1;
            }
            if(a.name > b.name) {
                return 1;
            }
            return 0;
        });
    }


}