import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: true //set to false will notice pipe change (e.g new element) when in filter mode
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filteredStatus: string, propName: string): any {
    if(value.length === 0 || !filteredStatus) {
      return value;
    }
    const resultArray = [];
    for(const item of value) {
      if(item[propName] === filteredStatus) {
        resultArray.push(item);
      }
    }
    return resultArray;
  }

}
